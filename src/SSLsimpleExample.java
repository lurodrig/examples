import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SSLsimpleExample extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		String urlString = req.getParameter("url");
		printLocalJVMconfiguration(out);
		getHttpResponse(urlString, out);
	}

	private void printLocalJVMconfiguration(PrintWriter out) {
		out.println("****** Local JVM configuration ********");
		String javaHome = System.getProperty("java.home");
		String trustStorePath = System.getProperty("javax.net.ssl.trustStore");
		out.println("System.property(java.home) : " + javaHome);
		out.println("System.property(catalina.base) : "
				+ System.getProperty("catalina.base"));
		out.println("System.property(catalina.home) : "
				+ System.getProperty("catalina.home"));
		out.println("System.property(javax.net.ssl.trustStore) : "
				+ trustStorePath);
		File trustStoreFile = null;
		if (trustStorePath == null) {
			out.println("javax.net.ssl.trustStore not set. Checking JSSE defaults: ");
			trustStoreFile = new File(javaHome + "/lib/security/jssecacerts");
			if (trustStoreFile.exists()) {
				out.println("Using: " + trustStoreFile.getPath()
						+ " as TrustStore");
			} else {
				trustStoreFile = new File(javaHome + "/lib/security/cacerts");
				if (trustStoreFile.exists()) {
					out.println("Using: " + trustStoreFile.getPath()
							+ " as TrustStore");
				}
			}
		} else {
			trustStoreFile = new File(trustStorePath);
			if (trustStoreFile.exists()) {
				out.println("Using: " + trustStoreFile.getPath()
						+ " as TrustStore");
			} else {
				out.println(trustStorePath
						+ " declared as javax.net.ssl.trustStore but FILE DOES NOT EXIST!!!");
			}
		}
		out.println();
	}

	private static void getHttpResponse(String urlString, PrintWriter out)
			throws MalformedURLException, IOException {
		URL url = new URL(urlString);
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		print_https_cert(con, out);
		print_content(con, out);
		con.disconnect();
	}

	private static void print_https_cert(HttpsURLConnection con, PrintWriter out) {
		if (con != null) {
			try {
				out.println("****** Remote server Response ********");
				out.println("Response code: " + con.getResponseCode());
				out.println("Response headers: " + con.getResponseCode());
				Map<String, List<String>> map = con.getHeaderFields();
				for (Map.Entry<String, List<String>> entry : map.entrySet()) {
					out.println("Header name : " + entry.getKey() + " ,Header value : "
							+ entry.getValue());
				}
				out.println("Cipher Suite : " + con.getCipherSuite());
				out.println("\n");
				Certificate[] certs = con.getServerCertificates();
				if (certs != null && certs.length > 0) {
					out.println("This server is delivering "
							+ con.getServerCertificates().length
							+ " certificates : ");
					out.println();
					for (int i = 0; i < certs.length; i++) {
						X509Certificate certificate = (X509Certificate) certs[i];
						out.println("Certificate " + i);
						out.println(" Type: " + certificate.getType());
						out.println(" Serial number: " + certificate.getSerialNumber());
						out.println(" Subject: " + certificate.getSubjectDN().toString());
						out.println(" Issuer: " + certificate.getIssuerDN().toString());
						out.println();
					}
				} else {
					out.println("No certificate is delivered by this server!!!");
				}

			} catch (SSLPeerUnverifiedException e) {
				out.println(e.getMessage());
			} catch (IOException e) {
				out.println(e.getMessage());
			}
		}
	}

	private static void print_content(HttpsURLConnection con, PrintWriter out) {
		if (con != null) {
			try {
				out.println("****** Content of the URL ********");
				BufferedReader br = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String input;
				while ((input = br.readLine()) != null) {
					out.println(input);
				}
				br.close();
			} catch (IOException e) {
				out.println(e.getMessage());
			}
		}
	}

	public static void main(String[] args) throws MalformedURLException,
			IOException {
		PrintWriter out = new PrintWriter(System.out);
		out.println("BEGIN");
		// getHttpResponse(
		// "https://aiatlas016.cern.ch:443/ESHadoop/ES.jsp?guid=4E5EDBC7-5F6D-E111-B20A-003048F3524E",
		// out);
		getHttpResponse("https://twiki.cern.ch", out);
		out.println("END");
		out.flush();
		out.close();
	}

}
