import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import util.Constants;

public class DatabaseExample extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2272023433497993354L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		Connection connection = null;
		PreparedStatement statement = null;
		int result = 0;
		try {
			// Get connection
			connection = getConnection();
			// Query the current state of the container
			String queryState = "SELECT * FROM petclinic.owners";
			statement = connection.prepareStatement(queryState);
			// Execute the queryState
			ResultSet resultSet = statement.executeQuery();
			if (resultSet != null) {
				out.println("PET OWNERS");
				while (resultSet.next()) {
					out.print(resultSet.getInt("id"));
					out.print("-");
					out.print(resultSet.getString("first_name"));
					out.print(" ");
					out.print(resultSet.getString("last_name"));
					out.print(" ");
					out.print(resultSet.getString("address"));
					out.print(" ");
					out.print(resultSet.getString("city"));
					out.print(" ");
					out.print(resultSet.getString("telephone"));
					out.println();
				}
			}
		} catch (NamingException ex) {
			ex.printStackTrace(out);
		} catch (SQLException ex) {
			ex.printStackTrace(out);
		} finally {
			try {
				statement.close();
			} catch (Exception e) {
				e.printStackTrace(out);
			}
			try {
				connection.close();
			} catch (Exception e) {
				e.printStackTrace(out);
			}
		}
	}

	private Connection getConnection() throws NamingException, SQLException {
		Context initContext = new InitialContext();
		Context envContext = (Context) initContext
				.lookup(Constants.ENVIRONMENT_CONTEXT);
		String dataSourceId = Constants.DATASOURCE_ID;
		DataSource dataSource = (DataSource) envContext.lookup(dataSourceId);
		return dataSource.getConnection();
	}
}
