import java.io.IOException;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SystemPropertiesAndJvmFlagsExample extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		// Show JVM flags first
		out.println("----BEGIN JVM FLAGS----");
		RuntimeMXBean mxbean = ManagementFactory.getRuntimeMXBean();
		List<String> jvm_runtime_options =  mxbean.getInputArguments();
		for (Iterator iterator = jvm_runtime_options.iterator(); iterator
				.hasNext();) {
			String option = (String) iterator.next();
			out.println(option);
		}
		out.println("----END JVM FLAGS----");
		out.println();
		// Now print the system properties
		System.getProperties().list(out);
	}

	
}
